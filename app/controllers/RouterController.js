"use strict";

const fs = require("fs");
const { promisify } = require("util");
const readFileAsync = promisify(fs.readFile);

class RouterController {
    
    async getPage(req, res) {
        let page;

        try {
            page = await readFileAsync("./templates/index.html");
        } catch ( err ) {
            res.statusCode = 404;
            res.end("Resource not found!");
        }

        res.setHeader("Content-Type", "text/html");
        res.end(page);
    }

};

const routerController = new RouterController();
module.exports = routerController;