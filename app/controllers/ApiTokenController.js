"use strict";
const apiKey = "BY197AZD6K7YTXPRC31P5MRWBFHKU1AMCN";
const network = "ropsten";
const timeout = 3000;

const etherscanApi = require('etherscan-api').init(apiKey, network, timeout)

class TokenController {

    async getInfoByAddr(req, res) {
        const addr = req.params["addr"];
        const tokentx = await etherscanApi.account.tokentx(addr, null, 1, 'latest', 'asc');

        let tokens = {};

        tokentx.result.map((txns, index) => {
            let { tokenName, tokenSymbol, value, to } = txns;
            let nameToken = `${tokenName} (${tokenSymbol})`; 

            if ( nameToken in tokens ) {
                
                if ( to.toLowerCase() == addr.toLowerCase() ) {
                    tokens[nameToken].balance += +value;
                } else {
                    tokens[nameToken].balance -= +value;
                }

            } else {
                tokens[nameToken] = {};
                tokens[nameToken].name = nameToken;
                tokens[nameToken].key = index;

                if ( to.toLowerCase() === addr.toLowerCase() ) {
                    tokens[nameToken].balance = +value;
                } else {
                    tokens[nameToken].balance = 0;
                    tokens[nameToken].balance += +value;
                }
            }

        });

        res.json(tokens);
    }

};

const tokenController = new TokenController();
module.exports = tokenController;