"use strict"
const express = require("express");
const RouterController = require("./app/controllers/RouterController");
const ApiTokenController = require("./app/controllers/ApiTokenController");

const app = express();

app.use(express.json());
app.get("/getTokenInfoByAddr/:addr", ApiTokenController.getInfoByAddr);

app.use("/static", express.static("dist"));
app.use("/", RouterController.getPage);

const port = 8080;
app.listen(port);

console.log(`Application is started! Port: ${port}`);