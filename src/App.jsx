import React from "react";
import Main from "./ui/Main";
import {BrowserRouter, Route} from "react-router-dom";

function App() {
    return (
        <BrowserRouter>
            <div>
                <Route exact path="/">
                    <Main type="search" />
                </Route>
                <Route path="/:addr">
                    <Main type="wallet" />
                </Route>
            </div>
        </BrowserRouter> 
    );
};

export default App;
