"use strict";

export default class AjaxService {

    static onload(resolve, reject, xhr) {

        if ( xhr.status == 200 ) {
            resolve(xhr.response);
        } else {
            let error = new Error(xhr.statusText);

            error.code = xhr.status;
            reject(error);
        }
    }

    static onError(reject) {
        reject( new Error("Network Error") );
    }

    static get(url) {
        return new Promise( (resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open("GET", url, true);
            xhr.setRequestHeader("Content-Type", "application/json");

            xhr.onload = () => this.onload(resolve, reject, xhr);
            xhr.onerror = () => this.onError(reject);
            xhr.send();
        });
    }

};