import React from "react";
import { Input, Table, Modal } from "antd";
import AjaxService from "../services/AjaxService";
const { Search } = Input;

class Main extends React.Component{

    state = {
        showSearch: true,
        showCard: false,
        wallet: null,
        walletAddr: null
    }

    componentWillMount() {

        if ( this.props.type == "wallet" ) {
            this.setState({
                showSearch: false
            });
            let addr = window.location.pathname.slice(1);

            this.getWallet(addr);
        };
    }

    info(record) {
        Modal.info({
            title: record.name,
            content: (
                <div>
                    <p>Balance: {record.balance}</p>
                </div>
            ),
            onOk() {},
        });
    }

    async getWallet(addr) {
        const url = "/getTokenInfoByAddr/" + addr;
        const result = await AjaxService.get(url);
        let wallet = JSON.parse(result);

        if ( wallet ) {
            this.setState({
                wallet,
                showCard: true,
                walletAddr: addr
            });
        } else {
            this.setState({
                wallet: null,
                showCard: false,
                walletAddr: null
            });
        }
    }

    renderSearch() {

        if ( !this.state.showSearch) {
            return null;
        }


        return (
            <div
                style={{
                    margin: "15px"
                }}    
            >
                <Search
                    placeholder="0x..."
                    enterButton="Search"
                    size="large"
                    onSearch={value => this.getWallet(value)}
                />
            </div>
        ); 
    }

    renderTable() {
        
        if ( !this.state.showCard ) {
            return null;
        }

        let count = 0;
        const addr = this.state.walletAddr;
        const data = [];
        const columns = [
            {
              title: 'Token name',
              dataIndex: 'name'
            }
        ];

        for ( let key in this.state.wallet ) {
            count += 1;
            data.push(this.state.wallet[key]);
        }


        return (
            <div
                style={{
                    margin: "15px"
                }}
            >
                <h2><b>Addres:</b> {addr}</h2>
                <h2><b>Count tokens:</b> {count}</h2>
                <Table
                    onRow={(record, rowIndex) => {
                        return {
                            onClick: event => {this.info(record)}
                        };
                    }}
                    columns={columns}
                    dataSource={data}
                    bordered
                />
            </div>
        );
    }

    
    render() {
        
        return (
            <div 
                style={{
                    margin: "15px"
                }}>
                { this.renderSearch() }
                { this.renderTable() }
            </div>
        );
    }
};

export default Main;